#!/bin/bash
#VARIABLES A MODIFICAR
RUTA=$PWD
HOSTNAME_SRV_WS='s01-wsdev-001'
HOSTNAME_SRV_WEB='s01-appdev-001'
HOSTNAME_SRV_BD='s01-dbdev-001'
IP_SRV_WS='10.165.7.127'
IP_SRV_WEB='10.165.7.126'
IP_SRV_BD='10.165.7.128'
NOMBRE_DB='gestiondb_dev'


#INSTALANDO LO NECESARIO
sudo apt update -y 
sudo apt upgrade -y 
echo $IP_SRV_WS $HOSTNAME_SRV_WS > /etc/hosts
echo $IP_SRV_BD $HOSTNAME_SRV_BD >> /etc/hosts
echo $IP_SRV_WEB $HOSTNAME_SRV_WEB >> /etc/hosts
echo $HOSTNAME_SRV_BD > /etc/hostname
sudo apt install mysql-server -y
sudo sed -i "s/127.0.0.1/0.0.0.0/g" "/etc/mysql/mysql.conf.d/mysqld.cnf"  
sudo mysql < $RUTA/instala_db.sql
sudo mysql < $RUTA/gestiondb.sql
echo "........Instalando base de datos"
sudo service mysql restart 
echo "Base de datos Instalada correctamente"  

#sudo sed -i "s/10.10.1.254/${IP}/g" "/etc/netplan/00-installer-config.yaml"
sudo reboot

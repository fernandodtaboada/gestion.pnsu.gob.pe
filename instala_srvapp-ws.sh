#!/bin/bash
#VARIABLES A MODIFICAR
RUTA=$PWD
HOSTNAME_SRV_WS='s01-wsdev-001'
HOSTNAME_SRV_WEB='s01-appdev-001'
HOSTNAME_SRV_BD='s01-dbdev-001'
IP_SRV_WS='10.165.7.127'
IP_SRV_WEB='10.165.7.126'
IP_SRV_BD='10.165.7.128'
USUARIO_BD=
PASSWD_BD=
USER_WILDFLY='administrator'
PASSWD_WILDFLY='administrator,,123'


sudo apt update -y 
sudo apt upgrade -y 
sudo apt install -y openjdk-8-jdk
sudo echo '"JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre"'  >> /etc/environment
cd /tmp
wget https://download.jboss.org/wildfly/16.0.0.Final/wildfly-16.0.0.Final.tar.gz
tar -xzvf wildfly-16.0.0.Final.tar.gz
sudo mv wildfly-16.0.0.Final/ /opt/wildfly
sudo groupadd -r wildfly
sudo useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly
sudo chown -RH wildfly: /opt/wildfly
sudo mkdir -p /etc/wildfly
cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/ 
cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh /opt/wildfly/bin/

sudo sh -c 'chmod +x /opt/wildfly/bin/*.sh'
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/ 
sudo systemctl stop wildfly.service
sudo systemctl start wildfly.service
sudo systemctl enable wildfly.service
#sudo systemctl status wildfly.service
sudo /opt/wildfly/bin/add-user.sh  -u '$USER_WILDFLY' -p '$PASSWD_WILDFLY'
echo $IP_SRV_WS $HOSTNAME_SRV_WS > /etc/hosts
echo $IP_SRV_BD $HOSTNAME_SRV_BD >> /etc/hosts
echo $IP_SRV_WEB $HOSTNAME_SRV_WEB >> /etc/hosts
echo $HOSTNAME_SRV_WS > /etc/hostname

wget https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.26/mysql-connector-java-8.0.26.jar
mkdir -p /opt/wildfly/modules/system/layers/base/com/mysql/main
cp $RUTA/mysql-connector-java-8.0.26.jar /opt/wildfly/modules/system/layers/base/com/mysql/main 
cp $RUTA/module.xml  /opt/wildfly/modules/system/layers/base/com/mysql/main
cp $RUTA/standalone.xml   /opt/wildfly/standalone/configuration/

sudo sed -i "s/ws-gestion/${HOSTNAME_SRV_WS}/g" "/opt/wildfly/standalone/configuration/standalone.xml"
sudo sed -i "s/127.0.0.1/${HOSTNAME_SRV_WS}/g" "/opt/wildfly/standalone/configuration/standalone-full.xml"
#sudo sed -i "s/10.10.1.254/${IP_SRV_WS}/g" "/etc/netplan/00-installer-config.yaml"
#sudo reboot
